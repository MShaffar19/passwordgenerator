package com.vecturagames.android.app.passwordgenerator.activity

import android.os.Bundle
import android.view.View
import android.widget.TextView

import androidx.appcompat.app.AppCompatActivity

import com.vecturagames.android.app.passwordgenerator.R
import com.vecturagames.android.app.passwordgenerator.preference.AppSettings
import com.vecturagames.android.app.passwordgenerator.util.Util

class AboutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setTheme(AppSettings.instance.appThemeId)
        setContentView(R.layout.activity_about)

        val textViewWebsiteValue = findViewById<View>(R.id.textViewWebsiteValue) as TextView
        textViewWebsiteValue.setOnClickListener { Util.openInternetBrowser(this@AboutActivity, getString(R.string.other_website), 0) }

        val textViewPrivacyPolicyValue = findViewById<View>(R.id.textViewPrivacyPolicyValue) as TextView
        textViewPrivacyPolicyValue.setOnClickListener { Util.openInternetBrowser(this@AboutActivity, getString(R.string.other_privacy_policy_website), 0) }

        val textViewEmailValue = findViewById<View>(R.id.textViewEmailValue) as TextView
        textViewEmailValue.setOnClickListener { Util.openEmailApplication(this@AboutActivity, getString(R.string.other_contact_email), 0) }

        val textViewSourceCodeValue = findViewById<View>(R.id.textViewSourceCodeValue) as TextView
        textViewSourceCodeValue.setOnClickListener { Util.openInternetBrowser(this@AboutActivity, getString(R.string.other_source_code_repository), 0) }

        val textViewVersionValue = findViewById<View>(R.id.textViewVersionValue) as TextView
        textViewVersionValue.text = Util.getVersionName(this)

        val textViewBuildNumberValue = findViewById<View>(R.id.textViewBuildNumberValue) as TextView
        val buildTime = Util.getBuildTime(this)
        val versionCode = Util.getVersionCode(this)

        if (versionCode > -1) {
            textViewBuildNumberValue.text = versionCode.toString() + if (buildTime != "") " ($buildTime)" else ""
        }
        else {
            textViewBuildNumberValue.text = getString(R.string.other_not_available)
        }
    }

}
