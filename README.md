# Password Generator
Password Generator is a simple Android application which generates secure passwords.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png" alt="Get it on F-Droid" height="80">](https://f-droid.org/packages/com.vecturagames.android.app.passwordgenerator/)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png" alt="Get it on Google Play" height="80">](https://play.google.com/store/apps/details?id=com.vecturagames.android.app.passwordgenerator)
